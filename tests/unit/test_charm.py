"""Unit tests for the charm."""

import unittest
from unittest.mock import PropertyMock, patch

import ops
import ops.testing
import pytest
from charm import CharmedDjangoCharm


class TestCharm(unittest.TestCase):
    def setUp(self):
        self.harness = ops.testing.Harness(CharmedDjangoCharm)
        self.addCleanup(self.harness.cleanup)

    @patch(
        "charm.CharmedDjangoCharm._secret_key",
        new_callable=PropertyMock,
        return_value="foo-bar-123",
    )
    @patch(
        "charm.CharmedDjangoCharm._database_url",
        new_callable=PropertyMock,
        return_value="dummy://",
    )
    def test_charmed_django_pebble_ready(self, mock_dburl, mock_seckey):
        """Is plan with default config in place after Pebble ready?

        Activities:
        1. Simulate the container coming up and emission of pebble-ready event
        2. Get the plan now we've run PebbleReady

        Checks:
        1. Have we got the plan we expected
        2. Was the service started
        3. Did we set an ActiveStatus
        """
        expected_plan = {
            "services": {
                "charmed-django": {
                    "override": "replace",
                    "summary": "charmed-django",
                    "command": "gunicorn application.wsgi",
                    "startup": "enabled",
                    "environment": {
                        "DJANGO_ALLOWED_HOSTS": "",
                        "DJANGO_DATABASE_URL": "dummy://",
                        "DJANGO_SECRET_KEY": "foo-bar-123",
                        "GUNICORN_CMD_ARGS": (
                            "--log-level=info"
                            " --access-logfile=-"
                            " --error-logfile=-"
                            " --bind=0.0.0.0:8000"
                            " --worker-class=gthread"
                            " --worker-tmp-dir=/dev/shm"
                            " --workers=2"
                            " --threads=4"
                        ),
                    },
                }
            },
        }

        self.harness.begin()
        self.harness.container_pebble_ready("charmed-django")
        updated_plan = self.harness.get_container_pebble_plan("charmed-django").to_dict()

        self.assertEqual(expected_plan, updated_plan)

        service = self.harness.model.unit.get_container("charmed-django").get_service(
            "charmed-django"
        )

        self.assertTrue(service.is_running())
        self.assertIsInstance(self.harness.model.unit.status, ops.ActiveStatus)

    @patch(
        "charm.CharmedDjangoCharm._secret_key",
        new_callable=PropertyMock,
        return_value="foo-bar-123",
    )
    @patch(
        "charm.CharmedDjangoCharm._database_url",
        new_callable=PropertyMock,
        return_value="dummy://",
    )
    def test_config_changed_valid_can_connect(self, mock_dburl, mock_seckey):
        """Do configuration changes propagate successfully?

        Activities:
        1. Ensure the simulated Pebble API is reachable
        2. Trigger a config-changed event with an updated value
        3. Get the plan now we've run PebbleReady

        Checks:
        1. Check the config change was effective
        """
        self.harness.begin()
        self.harness.set_can_connect("charmed-django", True)
        self.harness.update_config({"log-level": "debug"})
        self.harness.update_config({"domain-names": "example.com,foo.site"})

        updated_plan = self.harness.get_container_pebble_plan("charmed-django").to_dict()
        updated_env = updated_plan["services"]["charmed-django"]["environment"]

        self.assertEqual(
            updated_env,
            {
                "DJANGO_ALLOWED_HOSTS": "example.com,foo.site",
                "DJANGO_DATABASE_URL": "dummy://",
                "DJANGO_SECRET_KEY": "foo-bar-123",
                "GUNICORN_CMD_ARGS": (
                    "--log-level=debug"
                    " --access-logfile=-"
                    " --error-logfile=-"
                    " --bind=0.0.0.0:8000"
                    " --worker-class=gthread"
                    " --worker-tmp-dir=/dev/shm"
                    " --workers=2"
                    " --threads=4"
                ),
            },
        )
        self.assertIsInstance(self.harness.model.unit.status, ops.ActiveStatus)

    def test_config_changed_valid_cannot_connect(self):
        """Unit status must be WaitingStatus after a valid configuration update.

        Activities:
        1. Trigger a config-changed event with an updated value

        Checks:
        1. Check the charm is in WaitingStatus
        """
        self.harness.begin()
        self.harness.update_config({"log-level": "debug"})
        self.assertIsInstance(self.harness.model.unit.status, ops.WaitingStatus)

    def test_config_changed_invalid(self):
        """Unit status must be BlockedStatus after an invalid configuration update.

        Activities:
        1. Ensure the simulated Pebble API is reachable
        2. Trigger a config-changed event with an updated value

        Checks:
        1. Check the charm is in BlockedStatus
        """
        self.harness.begin()
        self.harness.update_config({"log-level": "foobar"})
        self.assertIsInstance(self.harness.model.unit.status, ops.BlockedStatus)

    @patch(
        "charm.CharmedDjangoCharm._secret_key",
        new_callable=PropertyMock,
        return_value="foo-bar-123",
    )
    def test_postgresql_relation_missing(self, mock_seckey):
        """Unit status must be WaitingStatus with missing database relation.

        Activities:
        1. Make sure database relation is missing

        Checks:
        1. Check the charm is in WaitingStatus
        """
        with pytest.raises(SystemExit):
            self.harness.begin_with_initial_hooks()
        self.assertIsInstance(self.harness.model.unit.status, ops.WaitingStatus)

    @patch(
        "charm.CharmedDjangoCharm._secret_key",
        new_callable=PropertyMock,
        return_value="foo-bar-123",
    )
    def test_postgresql_relation(self, mock_seckey):
        """Unit status must be ActiveStatus with database relation.

        Activities:
        1. Add a relation to a database
        2. Set the relation configuration

        Checks:
        1. Check the charm is in ActiveStatus
        2. Check the database URL is correct
        """
        self._add_postgresql_relation_with_data()
        self.harness.begin_with_initial_hooks()
        self.assertIsInstance(self.harness.model.unit.status, ops.ActiveStatus)

        plan = self.harness.get_container_pebble_plan("charmed-django").to_dict()
        env = plan["services"]["charmed-django"]["environment"]
        self.assertEqual(
            env["DJANGO_DATABASE_URL"],
            "postgresql://charmed-username:charmed-password@postgresql.example.com:5432/charmed-django",
        )

    @patch(
        "charm.CharmedDjangoCharm._secret_key",
        new_callable=PropertyMock,
        return_value="foo-bar-123",
    )
    def test_postgresql_relation_removed(self, mock_seckey):
        """Unit status must be WaitingStatus when the database relation is removed.

        Activities:
        1. Add a relation to a database
        2. Remove the relation

        Checks:
        1. Check the charm is in WaitingStatus
        """
        rel_id = self._add_postgresql_relation_with_data()
        self.harness.begin_with_initial_hooks()
        self.assertIsInstance(self.harness.model.unit.status, ops.ActiveStatus)

        with pytest.raises(SystemExit):
            self.harness.remove_relation(rel_id)
        self.assertIsInstance(self.harness.model.unit.status, ops.WaitingStatus)

    def _add_postgresql_relation_with_data(self):
        """Add a relation to a database and return the relation id."""
        rel_id = self.harness.add_relation("database", "postgresql")
        self.harness.add_relation_unit(rel_id, "postgresql/0")
        data = {
            "database": "charmed-django",
            "endpoints": "postgresql.example.com:5432",
            "username": "charmed-username",
            "password": "charmed-password",
        }
        self.harness.update_relation_data(rel_id, "postgresql", data)
        return rel_id

    def test_postgresql_relation_without_data(self):
        """Charm ignores relationship without data.

        Activities:
        1. Add a relation to a database

        Checks:
        1. Check the charm is in WaitingStatus
        """
        rel_id = self.harness.add_relation("database", "postgresql")
        self.harness.add_relation_unit(rel_id, "postgresql/0")

        with pytest.raises(SystemExit):
            self.harness.begin_with_initial_hooks()
        self.assertIsInstance(self.harness.model.unit.status, ops.WaitingStatus)

    @patch(
        "charm.CharmedDjangoCharm._generate_random_key",
        return_value="not-very-secret-key",
    )
    @patch(
        "charm.CharmedDjangoCharm._database_url",
        new_callable=PropertyMock,
        return_value="dummy://",
    )
    def test_secret_key_generated(self, mock_dburl, mock_genseckey):
        """Pebble plan contains secret key.

        Activities:
        1. Fire the install event

        Checks:
        1. Check the secret key is correct
        """
        self.harness.begin()
        self.harness.charm.on.install.emit()
        self.harness.container_pebble_ready("charmed-django")
        self.assertIsInstance(self.harness.model.unit.status, ops.ActiveStatus)

        plan = self.harness.get_container_pebble_plan("charmed-django").to_dict()
        env = plan["services"]["charmed-django"]["environment"]
        self.assertEqual(env["DJANGO_SECRET_KEY"], "not-very-secret-key")

        assert mock_dburl.called
        assert mock_genseckey.called

    @patch(
        "charm.CharmedDjangoCharm._database_url",
        new_callable=PropertyMock,
        return_value="dummy://",
    )
    def test_secret_key_missing(self, mock_genseckey):
        """Unit status must be WaitingStatus when the secret key is missing.

        Checks:
        1. Check the charm is in WaitingStatus
        """
        self.harness.begin()
        with pytest.raises(SystemExit):
            self.harness.container_pebble_ready("charmed-django")
        self.assertIsInstance(self.harness.model.unit.status, ops.WaitingStatus)

    @patch(
        "charm.CharmedDjangoCharm._database_url",
        new_callable=PropertyMock,
        return_value="dummy://",
    )
    def test_secret_key_generated_twice_ignored(self, mock_dburl):
        """Secret key is not generated twice.

        Activities:
        1. Fire the install event
        1. Fire the install event again

        Checks:
        1. Check the secret key is the same
        """
        self.harness.begin()
        self.harness.charm.on.install.emit()
        self.harness.container_pebble_ready("charmed-django")
        self.assertIsInstance(self.harness.model.unit.status, ops.ActiveStatus)

        plan = self.harness.get_container_pebble_plan("charmed-django").to_dict()
        env = plan["services"]["charmed-django"]["environment"]
        secret_key = env["DJANGO_SECRET_KEY"]

        self.harness.charm.on.install.emit()
        plan = self.harness.get_container_pebble_plan("charmed-django").to_dict()
        env = plan["services"]["charmed-django"]["environment"]
        self.assertEqual(env["DJANGO_SECRET_KEY"], secret_key)
