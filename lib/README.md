# Charm Libraries

This folder contains charm libraries for development.

To update the contained files run `tox -e update-libs` from time to time.

## See also

- [How to find and use a charm library](https://juju.is/docs/sdk/find-and-use-a-charm-library)
- [Integrate your charm with PostgreSQL](https://juju.is/docs/sdk/integrate-your-charm-with-postgresql)
