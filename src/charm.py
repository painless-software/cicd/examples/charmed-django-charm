#!/usr/bin/env python3
"""Charm the service."""

import logging
import secrets
import string

import ops

# Import `data_interfaces` library. The import statement omits the top-level `lib`
# directory, because `charmcraft pack` copies its contents to the project root.
from charms.data_platform_libs.v0.data_interfaces import DatabaseRequires, DatabaseRequiresEvent
from ops.model import SecretNotFoundError

# Log messages can be retrieved using `juju debug-log`
logger = logging.getLogger(__name__)

VALID_LOG_LEVELS = ["info", "debug", "warning", "error", "critical"]


class CharmedDjangoCharm(ops.CharmBase):
    """Charm the service."""

    def __init__(self, *args):
        super().__init__(*args)
        self.name = "charmed_django"
        self.framework.observe(
            self.on.charmed_django_pebble_ready, self._on_charmed_django_pebble_ready
        )
        self.framework.observe(self.on.config_changed, self._on_config_changed)

        # `relation_name` comes from the `metadata.yaml` file.
        # `database_name` is the name of the database that our application requires.
        self.database = DatabaseRequires(
            self, relation_name="database", database_name="charmed-django"
        )

        # See https://charmhub.io/data-platform-libs/libraries/data_interfaces
        self.framework.observe(self.database.on.database_created, self._on_database_relation)
        self.framework.observe(self.database.on.endpoints_changed, self._on_database_relation)
        self.framework.observe(
            self.on.database_relation_broken, self._on_database_relation_removed
        )

        # Generate the secret key on first unit install; treat a change as a reconfiguration
        self.framework.observe(self.on.install, self._create_secret_key)
        self.framework.observe(self.on.secret_changed, self._on_config_changed)

    def _create_secret_key(self, event):
        """Create a Django secret key and add it as an application secret."""
        try:
            self.model.get_secret(label="secret-key")
            return
        except SecretNotFoundError:
            pass

        key = self._generate_random_key()
        logging.debug("Creating new secret key: %s", key)
        self.app.add_secret({"key": key}, label="secret-key")

    @staticmethod
    def _generate_random_key(key_length=50):
        """Generate a secret just like Django's ``get_random_secret_key()``."""
        allowed_chars = string.ascii_letters + string.digits + "!?§$%&#()[]°^~=*/+-"
        return "".join([secrets.choice(allowed_chars) for _ in range(key_length)])

    def _on_charmed_django_pebble_ready(self, event: ops.PebbleReadyEvent):
        """Define and start a workload using the Pebble API.

        Change this example to suit your needs. You'll need to specify the right
        entrypoint and environment configuration for your specific workload.

        1. Get a reference the container attribute on the PebbleReadyEvent.
        2. Add initial Pebble config layer using the Pebble API.
        3. Make Pebble reevaluate its plan, ensuring any services are started if enabled.
        4. Report status to the Juju controller.

        More about statuses: https://juju.is/docs/sdk/constructs#heading--statuses
        More about interacting with Pebble: https://juju.is/docs/sdk/pebble
        """
        container = event.workload
        container.add_layer("charmed-django", self._pebble_layer, combine=True)
        container.replan()
        self.unit.status = ops.ActiveStatus("Active")

    def _on_config_changed(self, event: ops.ConfigChangedEvent):
        """Handle changed configuration.

        Change this example to suit your needs. If you don't need to handle config,
        you can remove this method.

        Learn more about config at https://juju.is/docs/sdk/config
        """
        self._update_layer_and_restart(event)

    def _update_layer_and_restart(self, event: ops.HookEvent) -> None:
        """Define and start a workload using the Pebble API.

        You'll need to specify the right entrypoint and environment
        configuration for your specific workload. Tip: you can see the
        standard entrypoint of an existing container using docker inspect.

        Learn more about Pebble layers at https://github.com/canonical/pebble
        """
        self.unit.status = ops.MaintenanceStatus("Assembling pod spec")

        log_level = self.model.config["log-level"].lower()
        domain_names = self.model.config["domain-names"].lower()

        if log_level in VALID_LOG_LEVELS:
            container = self.unit.get_container("charmed-django")

            if container.can_connect():
                container.add_layer("charmed-django", self._pebble_layer, combine=True)
                container.replan()

                logger.debug("Log level for gunicorn set to '%s'", log_level)
                logger.debug(
                    "Domain names for Django app set to '%s'",
                    domain_names,
                )
                self.unit.status = ops.ActiveStatus("Active")
            else:
                event.defer()
                self.unit.status = ops.WaitingStatus("waiting for Pebble API")
        else:
            self.unit.status = ops.BlockedStatus(f"invalid log level: '{log_level}'")

    def _on_database_relation(self, event: DatabaseRequiresEvent) -> None:
        """Event is fired when Postgres database is created or changed."""
        self._update_layer_and_restart(event)

    def _on_database_relation_removed(self, event) -> None:
        """Event is fired when relation with Postgres is broken."""
        self.unit.status = ops.WaitingStatus("Waiting for database relation")
        raise SystemExit(0)

    @property
    def _database_url(self):
        """Return the database connection string.

        Retrieves the database authentication data from the Juju controller
        and uses it to construct the database connection string.
        """
        relations = self.database.fetch_relation_data()
        logger.debug("Got following database data: %s", relations)
        for data in relations.values():
            if not data:
                continue
            logger.info("PostgreSQL database endpoint is %s", data["endpoints"])
            return f"postgresql://{data['username']}:{data['password']}@{data['endpoints']}/{data['database']}"
        self.unit.status = ops.WaitingStatus("Waiting for database relation")
        raise SystemExit(0)

    @property
    def _secret_key(self):
        """Return the secret key from the application secret."""
        try:
            secret = self.model.get_secret(label="secret-key")
        except SecretNotFoundError:
            self.unit.status = ops.WaitingStatus("Waiting for secret key")
            raise SystemExit(0)
        content = secret.get_content()
        logging.debug("Got secret-key content: %s", content)
        return content["key"]

    @property
    def _pebble_layer(self):
        """Return a dictionary representing a Pebble layer."""
        return {
            "summary": "charmed-django layer",
            "description": "pebble config layer for charmed-django",
            "services": {
                "charmed-django": {
                    "override": "replace",
                    "summary": "charmed-django",
                    "command": "gunicorn application.wsgi",
                    "startup": "enabled",
                    "environment": {
                        "DJANGO_ALLOWED_HOSTS": self.model.config["domain-names"],
                        "DJANGO_DATABASE_URL": self._database_url,
                        "DJANGO_SECRET_KEY": self._secret_key,
                        "GUNICORN_CMD_ARGS": (
                            f"--log-level={self.model.config['log-level']}"
                            " --access-logfile=-"
                            " --error-logfile=-"
                            " --bind=0.0.0.0:8000"
                            " --worker-class=gthread"
                            " --worker-tmp-dir=/dev/shm"
                            " --workers=2"
                            " --threads=4"
                        ),
                    },
                }
            },
        }


if __name__ == "__main__":  # pragma: nocover
    ops.main(CharmedDjangoCharm)
