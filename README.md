# Charmed Django

Orchestration for Charmed Django.

This project deploys the containerised `charmed-django` application on Kubernetes.
See the [Releases](#releases) section below for details.

## Getting Started

See [CONTRIBUTING](CONTRIBUTING.md) for instructions on development and
deployment.

## Code Changes

1. Start a topic branch.
1. Make code changes and add related tests.
1. Open a merge request.

For running tests, linting, security checks, etc. see instructions in the
[tests](tests) folder.

## Releases

The CI/CD pipeline will deploy the application to the target environments.
We have 3 environments corresponding to 3 namespaces on our container
platform: *development*, *integration*, *production*

- Any merge request triggers a deployment of a review app on *development*.
  When a merge request is merged or closed the review app will automatically
  be removed.
- Any change on the main branch, e.g. when a merge request is merged into
  `main`, triggers a deployment on *integration*.
- To trigger a deployment on *production* create a Release or push a Git
  tag, e.g.

  ```shell
  git checkout main
  git tag 1.0.0
  git push --tags
  ```
