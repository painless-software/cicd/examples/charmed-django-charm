# Contributing

To make contributions to this charm, you'll need a working
[development setup](https://juju.is/docs/sdk/dev-setup).

Alternatively, you can use the provided Vagrantfile to run an Ubuntu machine
with all Juju charm development tools installed:

```shell
vagrant up
vagrant ssh
```

You can create an environment for development with `tox`:

```shell
tox devenv -e integration
source venv/bin/activate
```

## Testing

This project uses `tox` for managing test environments. There are some
pre-configured environments that can be used for linting and formatting
code when you're preparing contributions to the charm:

```shell
tox run -e format        # update your code according to linting rules
tox run -e lint          # code style
tox run -e unit          # unit tests
tox run -e integration   # integration tests
tox                      # runs 'format', 'lint', and 'unit' environments
```

## Build the charm

Build the charm in this git repository using:

```shell
charmcraft pack --destructive-mode
```

See the [Juju SDK documentation](https://juju.is/docs/sdk) for more
information about developing and improving charms.

## Deploy the charm

You need to run the application on a Kubernetes cluster.

### Prerequisites

Given you use microk8s, the setup steps might be as follows:

1. Install and configure microk8s:

    ```shell
    sudo snap install microk8s --channel 1.28-strict/stable
    # afterwards, add local user to `snap_microk8s` group and reboot machine
    sudo microk8s enable hostpath-storage dns
    sudo snap install juju --channel 3.4/stable
    ```

2. Configure the application environment:

    ```shell
    juju bootstrap microk8s charmed-django
    juju add-model charmed-django
    ```

3. Build or pull the application container image:

    ```shell
    docker login registry.gitlab.com
    docker pull registry.gitlab.com/painless-software/cicd/examples/charmed-django:main
    ```

### Install or update the charm

```shell
juju deploy postgresql-k8s --channel=14/stable --trust
juju deploy ./*.charm --resource charmed-django=registry.gitlab.com/painless-software/cicd/examples/charmed-django:main
juju integrate postgresql-k8s charmed-django
```

An already deployed charm is updated using the `refresh` command, e.g.

```shell
juju refresh charmed-django --path ./*.charm
```

### Troubleshooting

Status inspection:

```shell
juju controllers
juju models
juju status --relations --watch 1s
```

Log analysis:

```shell
juju debug-log
juju show-status-log --show-log --debug --type application charmed-django
juju ssh --container charmed-django charmed-django/0 /charm/bin/pebble logs
microk8s.kubectl logs -n charmed-django -c charmed-django pod/charmed-django-0
```

See also the [Juju SDK docs](https://juju.is/docs/sdk/debug-a-charm) on
debugging a charm.

Cleaning up:

```shell
juju destroy-model charmed-django --no-prompt --force --no-wait
juju kill-controller charmed-django --no-prompt
juju unregister charmed-django --no-prompt
```

```shell
sudo microk8s stop
sudo snap remove --purge juju
sudo snap remove --purge microk8s
```
