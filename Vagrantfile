$script = <<-'SCRIPT'
#!/bin/bash
#
# Adapted from https://juju.is/docs/juju/get-started-with-juju

set -euo pipefail
set -x  # show commands to be executed

################################################################
#
# LXC / LXD
#
# 1. LXD is already installed on Ubuntu, just configure it
# 2. Add the user to the LXD group

sudo lxd init --auto
sudo lxc network set lxdbr0 ipv6.address none
sudo adduser $USER lxd

################################################################
#
# MicroK8S
#
# 1. Install MicroK8S from snap
# 2. Enable the necessary MicroK8s addons
# 3. Set up a short alias for the Kubernetes CLI
# 4. Add the user to the MicroK8s group

sudo snap install microk8s --channel 1.28-strict/stable
sudo microk8s enable hostpath-storage dns
sudo snap alias microk8s.kubectl kubectl
sudo adduser $USER snap_microk8s

# For the rest of this script, if we need the snap_microk8s group, explicitly
# use `sudo -u $USER <command>` or group membership will not be available.

################################################################
#
# Juju
#
# 1. Install Juju
# 2. Juju recognizes the LXD and MicroK8S clouds automatically, no need to
#    add them explicitly
# 3. Install the 'dev' Juju controller in the MicroK8S cloud
# 4. Add the 'dev' workspace ("model")
# 5. Change the kubectl context to the 'dev' namespace

sudo snap install juju --channel 3.4/stable
mkdir -p ~/.local/share
sudo -u $USER juju bootstrap microk8s dev
sudo -u $USER juju add-model dev
sudo -u $USER kubectl config set-context --current --namespace=dev

################################################################
#
# Charmcraft

sudo snap install charmcraft --classic

################################################################
#
# Python development

sudo apt-get update
sudo apt-get install -y python-is-python3 python3-venv
python -m venv venv
source venv/bin/activate
pip install -U pip
pip install tox
echo 'source venv/bin/activate && cd shared' > .bash_aliases

SCRIPT

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/jammy64"

  config.vm.provider "virtualbox" do |v|
    v.memory = 4096
    v.cpus = 2
  end

  # Snap binaries cannot usually escape the home directory, so mount shared
  # folder within
  config.vm.synced_folder ".", "/home/vagrant/shared"

  config.vm.provision :shell, inline: $script, privileged: false
end
